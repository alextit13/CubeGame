package Classes;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Map extends ImageView{
    private String imageMapPath;
    private ImageView imageViewMap;

    public ImageView MapPaint(){
        imageMapPath = "map.png";
        Image image = new Image(imageMapPath);
        imageViewMap = new ImageView(image);
        return imageViewMap;
    }
    public ImageView MapPaint_Montain(){
        imageMapPath = "map_montain.png";
        Image image = new Image(imageMapPath);
        imageViewMap = new ImageView(image);
        return imageViewMap;
    }

    @Override
    public String toString() {
        return "Map_" + imageMapPath;
    }
}
