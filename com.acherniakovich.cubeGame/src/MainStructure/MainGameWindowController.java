package MainStructure;

import Classes.Cube;
import Classes.Gamer;
import Classes.Map;
import Classes.PointLocated;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;

public class MainGameWindowController {

    public AnchorPane MapField;
    public HBox CubeField;
    public Label hodov;
    public Label ostalosHodov;
    public int hodi = 3;
    public int SUM_NUM = 0;
    public Gamer GAMER = new Gamer();
    public ImageView gamer;
    public ImageView nextStepImapeView;
    public ImageView newGame;
    public ImageView returnGameImage;
    public ImageView exit;
    public Label Vibros;
    public Label informationLabel;
    public javafx.scene.control.ChoiceBox ChoiceBox;
    public ObservableList mapList = FXCollections.observableArrayList();

    @FXML
    public void initialize(){
        Map map = new Map();

        mapList.add("Карта_1");
        mapList.add("Карта_2");
        ChoiceBox.setItems(mapList);
        MapField.getChildren().add(map.MapPaint());
    }

    public void ClicknextStepImapeView(MouseEvent mouseEvent) {
        CubeField.getChildren().clear();
        Cube cube1 = new Cube();
        CubeField.getChildren().add(cube1.CubeGenerate());
        Cube cube2 = new Cube();
        CubeField.getChildren().add(cube2.CubeGenerate());
        SUM_NUM = SUM_NUM + cube1.numberSend() + cube2.numberSend();//тут забираются суммы кубиков и складываются в SUM_NUM
        Vibros.setText(SUM_NUM + "");
        //тут наш герой будет двигаться
        PointLocated pl = new PointLocated();
        int [] arr = new int[2];
        arr = pl.PointCoordinate(SUM_NUM);
        //следующие два геймера нужно будет убрать
        gamer.setTranslateX(arr[0]);
        gamer.setTranslateY(arr[1]);
        if (hodi>0){
            hodi--;
            ostalosHodov.setText(hodi + "");
            hodov.setText(3 - hodi + "");
        }
        if (SUM_NUM == 6){
            gamer.setTranslateX(335);
            gamer.setTranslateY(65);
            informationLabel.setText("Неудача( Вы переходите на 3 шаг");

        }
        if (SUM_NUM == 9){
            hodi = 0;
            gamer.setTranslateX(85);
            gamer.setTranslateY(50);
            informationLabel.setText("К сожалению, Вы сгорели(");
        }
        if (SUM_NUM == 10||SUM_NUM == 14){
            gamer.setTranslateX(615);
            gamer.setTranslateY(295);
            informationLabel.setText("Вы попали на счастливый шаг - 10! Вы выиграли!");
        }
        if (SUM_NUM>=14&&hodi<4){
            informationLabel.setText("Вы выиграли!");
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        double scale = 1.0;
                        while (scale<=2){
                            Thread.sleep(5);
                            gamer.setScaleX(scale);
                            gamer.setScaleY(scale);
                            scale += 0.01;
                        }
                        gamer.setRotate(180);
                        Thread.sleep(1000);

                        while (scale>=1){
                            Thread.sleep(5);
                            gamer.setScaleX(scale);
                            gamer.setScaleY(scale);
                            scale -= 0.02;
                        }

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };
            Thread thread = new Thread(runnable);
            thread.start();

        }
        if (SUM_NUM<14&&hodi<=0){
            informationLabel.setText("Вы проиграли( У Вас не осталось ходов");
        }
    }

    public void clickNewGame(MouseEvent mouseEvent) {
        if (ChoiceBox.getValue() == "Карта_2"){
            MapField.getChildren().clear();
            Map map = new Map();
            MapField.getChildren().add(map.MapPaint_Montain());
            gamer = GAMER.GamerPaintMeth();
            MapField.getChildren().add(gamer);
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        gamer.setScaleX(4);
                        gamer.setScaleY(4);
                        double scale = 4.0;
                        while (scale>=1){
                            Thread.sleep(5);
                            gamer.setScaleX(scale);
                            gamer.setScaleY(scale);
                            scale -= 0.01;
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };
            Thread thread = new Thread(runnable);
            thread.start();
            gamer.setTranslateX(85);
            gamer.setTranslateY(50);
        }else {
            MapField.getChildren().clear();
            Map map = new Map();
            MapField.getChildren().add(map.MapPaint());
            gamer = GAMER.GamerPaintMeth();
            MapField.getChildren().add(gamer);
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        gamer.setScaleX(4);
                        gamer.setScaleY(4);
                        double scale = 4.0;
                        while (scale>=1){
                            Thread.sleep(5);
                            gamer.setScaleX(scale);
                            gamer.setScaleY(scale);
                            scale -= 0.01;
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };
            Thread thread = new Thread(runnable);
            thread.start();
            gamer.setTranslateX(85);
            gamer.setTranslateY(50);
        }
    }

    public void clickreturnGameImage(MouseEvent mouseEvent) {
        hodi = 3;
        ostalosHodov.setText(hodi + "");
        hodov.setText(3 - hodi + "");
        SUM_NUM = 0;
        gamer.setTranslateX(85);
        gamer.setTranslateY(50);
        Vibros.setText(0 + "");
        informationLabel.setText("Бросайте кости!");
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    gamer.setScaleX(4);
                    gamer.setScaleY(4);
                    double scale = 4.0;
                    while (scale>=1){
                        Thread.sleep(5);
                        gamer.setScaleX(scale);
                        gamer.setScaleY(scale);
                        scale -= 0.01;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public void clickExit(MouseEvent mouseEvent) {
        System.exit(1);
    }

    public void EnteredNextStep(MouseEvent mouseEvent) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    double scale = 1.0;
                    while (scale<=1.2){
                        Thread.sleep(5);
                        nextStepImapeView.setScaleX(scale);
                        nextStepImapeView.setScaleY(scale);
                        scale += 0.01;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public void ExistedNextStep(MouseEvent mouseEvent) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    double scale = 1.2;
                    while (scale>=1.0){
                        Thread.sleep(5);
                        nextStepImapeView.setScaleX(scale);
                        nextStepImapeView.setScaleY(scale);
                        scale -= 0.01;
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public void EnteredNewGame(MouseEvent mouseEvent) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    double rotate = 0.0;
                    double scale = 1;
                    while (rotate<=10&&scale<=1.2){
                        Thread.sleep(5);
                        newGame.setRotate(rotate);
                        newGame.setScaleX(scale);
                        newGame.setScaleY(scale);
                        rotate++;
                        scale += 0.02;
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public void ExitedNewGame(MouseEvent mouseEvent) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    double rotate = 10.0;
                    double scale = 1.2;
                    while (rotate>=1&&scale>=1){
                        Thread.sleep(5);
                        newGame.setRotate(rotate);
                        newGame.setScaleX(scale);
                        newGame.setScaleY(scale);
                        rotate--;
                        scale -= 0.02;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public void EnteredResetGame(MouseEvent mouseEvent) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    double rotate = 0.0;
                    double scale = 1;
                    while (rotate<=10&&scale<=1.2){
                        Thread.sleep(5);
                        returnGameImage.setRotate(rotate);
                        returnGameImage.setScaleX(scale);
                        returnGameImage.setScaleY(scale);
                        rotate++;
                        scale += 0.02;
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public void ExitedResetGame(MouseEvent mouseEvent) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    double rotate = 10.0;
                    double scale = 1.2;
                    while (rotate>=1&&scale>=1){
                        Thread.sleep(5);
                        returnGameImage.setRotate(rotate);
                        returnGameImage.setScaleX(scale);
                        returnGameImage.setScaleY(scale);
                        rotate--;
                        scale -= 0.02;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public void EnteredExit(MouseEvent mouseEvent) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    double rotate = 0.0;
                    double scale = 1;
                    while (rotate<=10&&scale<=1.2){
                        Thread.sleep(5);
                        exit.setRotate(rotate);
                        exit.setScaleX(scale);
                        exit.setScaleY(scale);
                        rotate++;
                        scale += 0.02;
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public void ExitedExit(MouseEvent mouseEvent) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    double rotate = 10.0;
                    double scale = 1.2;
                    while (rotate>=1&&scale>=1){
                        Thread.sleep(5);
                        exit.setRotate(rotate);
                        exit.setScaleX(scale);
                        exit.setScaleY(scale);
                        rotate--;
                        scale -= 0.02;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }
}