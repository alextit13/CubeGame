package Classes;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Gamer extends ImageView{
    private ImageView ImageViewGamer;
    private Image gamerImage;
    private String gamerImagePath;

    public ImageView GamerPaintMeth(){
        gamerImagePath = "2.png";
        gamerImage = new Image(gamerImagePath);
        ImageViewGamer = new ImageView(gamerImage);
        return ImageViewGamer;
    }

}
