package Classes;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Cube extends ImageView{
    private ImageView imageViewCube;
    private Image imageCube;
    private int number;
    private String cubeNum;

    public ImageView CubeGenerate(){
        number = (int)(Math.random()*7);
        if (number == 0)
            number = 1;
        switch (number){
            case 0:
                cubeNum = "cube_1.png";
                break;
            case 1:
                cubeNum = "cube_1.png";
                break;
            case 2:
                cubeNum = "cube_2.png";
                break;
            case 3:
                cubeNum = "cube_3.png";
                break;
            case 4:
                cubeNum = "cube_4.png";
                break;
            case 5:
                cubeNum = "cube_5.png";
                break;
            case 6:
                cubeNum = "cube_6.png";
                break;
        }
        imageCube = new Image(cubeNum);
        imageViewCube = new ImageView(imageCube);
        return imageViewCube;
    }

    public int numberSend(){
        return number;
    }
}
