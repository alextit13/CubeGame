package Classes;

import java.util.HashMap;

public class PointLocated {
    private int arr[] = new int[2];
    private HashMap<Integer, int[]>MapCoordinate = new HashMap<>();
    public int[] PointCoordinate(int numberOfCubes){

        MapCoordinate.put(0,new int[]{85,50});
        MapCoordinate.put(1,new int[]{160,45});
        MapCoordinate.put(2,new int[]{250,55});
        MapCoordinate.put(3,new int[]{335,65});
        MapCoordinate.put(4,new int[]{400,110});
        MapCoordinate.put(5,new int[]{450,175});
        MapCoordinate.put(6,new int[]{530,55});
        MapCoordinate.put(7,new int[]{410,280});
        MapCoordinate.put(8,new int[]{310,285});
        MapCoordinate.put(9,new int[]{313,230});
        MapCoordinate.put(10,new int[]{260,300});
        MapCoordinate.put(11,new int[]{450,355});
        MapCoordinate.put(12,new int[]{510,420});
        MapCoordinate.put(13,new int[]{620,395});
        MapCoordinate.put(14,new int[]{615,295});

        if (numberOfCubes>=14){
            arr = MapCoordinate.get(14);
        }else {
            arr = MapCoordinate.get(numberOfCubes);
        }
        return arr;
    }
}
