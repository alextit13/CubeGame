package MainStructure;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("MainGameWindow.fxml"));
        primaryStage.setTitle("CubeGame");
        primaryStage.setScene(new Scene(root));
        primaryStage.setMinHeight(638);
        primaryStage.setMinWidth(817);
        primaryStage.setMaxHeight(638);
        primaryStage.setMaxWidth(817);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
